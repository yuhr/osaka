//
//  Config.swift
//  osaka
//
//  Created by yuhr on 2018/01/08.
//  Copyright © 2018 yuhr. All rights reserved.
//

import Foundation
import UIKit
import ARKit

class Config: UIPageViewController {
  
  var initial = ConfigInit()
  var debug = ConfigDebug()
  
  let beaconCluster = BeaconCluster()
  let beaconThresholdDistanceFarInit = 0.2
  let beaconThresholdDistanceNearInit = 0.15
  let beaconThresholdDistanceFarDebug = 0.15
  let beaconThresholdDistanceNearDebug = 0.1
  let beaconThresholdDurationInit = 1.0
  let beaconThresholdDurationDebug = 0.5
  var beaconTimeFirstNear: Date? = nil
  var beaconTimeFirstFar: Date? = nil
  
  var isBusy = false
  private var _isPerforming = false
  var isPerforming: Bool {
    get { return self._isPerforming }
    set(value) {
      if value != self._isPerforming {
        if value {
          DispatchQueue.main.async {
            if !self.isBusy {
              self.isBusy = true
              config.present(performance, animated: true, completion: {
                self._isPerforming = true
                print("isPerforming = \(value)")
                self.isBusy = false
              })
            }
          }
        } else {
          DispatchQueue.main.async {
            if !self.isBusy {
              self.isBusy = true
              self._isPerforming = false
              print("isPerforming = \(value)")
              performance.dismiss(animated: true, completion: {
                self.isBusy = false
              })
            }
          }
        }
      }
    }
  }
  
  enum Status {
    case normal
    case limited
    case error
    var string: String {
      switch self {
      case .normal: return "Normal"
      case .limited: return "Limited"
      case .error: return "Error"
      }
    }
    init(arCameraTrackingState: ARCamera.TrackingState) {
      switch arCameraTrackingState {
      case .normal: self = .normal
      case .limited(_): self = .limited
      case .notAvailable: self = .error
      }
    }
  }
  private var _status: Status = .normal
  var status: Status {
    get { return self._status }
    set(value) {
      if let label = self.debug.labels["Status"] {
        label.setText(value.string)
        switch value {
        case .normal:
          label.setColor(.white)
          performance.isWarning = false
        case .limited:
          label.setColor(.red)
          performance.isWarning = true
        case .error:
          label.setColor(.orange)
        }
      }
    }
  }
  
  var numFtPts: Int {
    get { return Int(debug.labels["NumFtPts"]!.text!)! }
    set(value) {
      self.debug.labels["NumFtPts"]!.setText(value)
    }
  }
  
  var lightness: Float {
    get { return Float(debug.labels["Lightness"]!.text!)! }
    set(value) {
      guard let label = self.debug.labels["Lightness"] else { return }
      if value == -1.0 {
        label.setText("N/A")
        label.setColor(.white)
      } else {
        label.setText(String(format: "%d lm", Int(round(value))))
        label.setColor(value < 100 ? .red : .white)
      }
    }
  }
  
  var distFromOrig: Float {
    get { return Float(debug.labels["FromOrig"]!.text!)! }
    set(value) {
      guard !self.isInitial else { return }
      if let label = debug.labels["FromOrig"] {
        if 40.0 < value {
          performance.isErrorOccured = true
          label.setColor(.orange)
        } else {
          label.setColor(.white)
        }
        label.setText(String(format: "%0.3f m", value.roundAtPlace(nth: 3)))
      }
    }
  }
  
  var distFromPrev: Float {
    get { return Float(debug.labels["FromPrev"]!.text!)! }
    set(value) {
      guard !self.isInitial else { return }
      if let label = debug.labels["FromPrev"] {
        if 5.0 < value {
          performance.isErrorOccured = true
          label.setColor(.orange)
        } else {
          label.setColor(.white)
        }
        label.setText(String(format: "%0.3f m", value.roundAtPlace(nth: 3)))
      }
    }
  }
  
  init() {
    super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    self.modalPresentationStyle = .overCurrentContext
    self.modalPresentationCapturesStatusBarAppearance = true
    self.modalTransitionStyle = .crossDissolve
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setViewControllers([self.initial], direction: .forward, animated: false)
  }
  
  func succeed() {
    self.initial.check()
  }
  
  var isInitial = true {
    didSet(oldValue) {
      if oldValue != self.isInitial {
        print("isInitial = \(self.isInitial)")
      }
    }
  }
  func restart() {
    print("restart()")
    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
    if self.isInitial {
      ar.stop()
      self.initial.isInitialized = false
      self.initial.isInitializing = false
    } else {
      self.isInitial = true
      ar.stop()
      self.initial.isInitialized = false
      self.initial.isInitializing = false
      Audio.isPlayable = false
      self.setViewControllers([self.initial], direction: .reverse, animated: false)
    }
  }
  
  func impact() {
    if !self.isPerforming {
      DispatchQueue.main.async {
        self.debug.feedbackImpact.impactOccurred()
      }
    }
  }
  
  override var childViewControllerForStatusBarStyle: UIViewController? { return performance }
  override var prefersStatusBarHidden: Bool { return false }
  required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
