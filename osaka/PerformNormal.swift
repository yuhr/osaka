//
//  PerformNormal.swift
//  osaka
//
//  Created by yuhr on 2017/12/26.
//  Copyright © 2017 yuhr. All rights reserved.
//

import Foundation
import UIKit
import Interpolate
//import AVFoundation

class PerformNormal: UIViewController {
  
  let colorNormal: UIColor = .black
  let colorWarning: UIColor = .red
  private var fade: Interpolate!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = self.colorNormal
    
    self.fade = Interpolate(from: self.colorNormal, to: self.colorWarning, apply: { color in
      self.view.backgroundColor = color
    })
  }
  
  private var _isWarning: Bool = false
  var isWarning: Bool {
    get { return self._isWarning }
    set(value) {
      if value != self._isWarning {
        self._isWarning = value
        /*
        if value {
          AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
        self.fade.stopAnimation()
        self.fade.animate(value ? 1.0 : 0.0, duration: 0.3)
         */
      }
    }
  }
}
