//
//  AR.swift
//  osaka
//
//  Created by yuhr on 2018/01/08.
//  Copyright © 2018 yuhr. All rights reserved.
//

import Foundation
import UIKit
import ARKit
import SceneKit

class AR: UIViewController, ARSCNViewDelegate {
  
  var viewScene: ARSCNView!
  var device: DeviceNode!
  var origin: OriginNode!
  var zDirection = SCNVector3(0.0, 0.0, -1.0)
  let arConfig = ARWorldTrackingConfiguration()
  var isStarting = false
  
  init() {
    super.init(nibName: nil, bundle: nil)
    self.modalTransitionStyle = .crossDissolve
    self.modalPresentationCapturesStatusBarAppearance = true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Setup AR
    self.viewScene = ARSCNView(frame: self.view.frame)
    self.view.addSubview(self.viewScene)
    self.viewScene.delegate = self
    self.viewScene.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
    
    self.arConfig.planeDetection = .horizontal
    self.arConfig.worldAlignment = .gravity
    
    // Create indicator nodes
    self.device = DeviceNode()
    self.origin = OriginNode()
    
    // Create a scene
    let scene = SCNScene()
    scene.rootNode.addChildNode(device)
    scene.rootNode.addChildNode(origin)
    for audio in Audio.list {
      scene.rootNode.addChildNode(audio.node)
    }
    self.viewScene.scene = scene
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  func start() {
    self.viewScene.session.run(self.arConfig, options: [.resetTracking, .removeExistingAnchors])
    self.origin.available = false
  }
  
  func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
    switch camera.trackingState {
    case .limited(let reason):
      switch reason {
      case .initializing:
        self.origin.available = false
        print("ARKit: Initializing")
      case .excessiveMotion:
        print("ARKit: Excessive motion")
      case .insufficientFeatures:
        print("ARKit: Insufficient features")
      case .relocalizing:
        print("ARKit: Relocalizing")
      }
    case .normal:
      print("ARKit: Normal")
      if !self.origin.available {
        self.origin.available = true
        performance.isWarning = false
        config.succeed()
        self.origin.position = SCNVector3Zero
        if let rotationY = self.viewScene.session.currentFrame?.camera.eulerAngles.y {
          self.zDirection = SCNVector3(sin(rotationY), 0.0, cos(rotationY)).normalized
          Audio.rearrange(at: self.origin.position, rotationY: rotationY)
        } else {
          self.zDirection = SCNVector3(0.0, 0.0, -1.0)
          Audio.rearrange(at: self.origin.position, rotationY: 0.0)
        }
      }
    case .notAvailable:
      print("ARKit: Not available")
    }
    config.status = Config.Status(arCameraTrackingState: camera.trackingState)
  }
  
  func stop() {
    self.viewScene.session.pause()
  }
  
  func renderer(_ renderer: SCNSceneRenderer, willRenderScene scene: SCNScene, atTime time: TimeInterval) {
    guard let pointOfView = self.viewScene.pointOfView else { return }
    if self.device.available {
      let prev = self.device.position
      let curr = pointOfView.position
      let d = curr - prev
      config.distFromPrev = d.length
    } else { self.device.available = true }
    self.device.position = pointOfView.position
    let d = self.origin.position - self.device.position
    config.distFromOrig = d.length
    
    Audio.deviceMoving(at: self.device.position)
    
    if let frame = self.viewScene.session.currentFrame {
      if let cloud = frame.rawFeaturePoints {
        config.numFtPts = cloud.points.count
      }
    }
    if let lightness = self.viewScene.session.currentFrame?.lightEstimate?.ambientIntensity {
      config.lightness = Float(lightness)
    } else {
      config.lightness = -1.0
    }
  }
  var isTooDark = false
  
  func session(_ session: ARSession, didFailWithError error: Error) {
    // Present an error message to the user
    print("session")
  }
  
  func sessionWasInterrupted(_ session: ARSession) {
    // Inform the user that the session has been interrupted, for example, by presenting an overlay
    print("sessionWasInterrupted")
  }
  
  func sessionInterruptionEnded(_ session: ARSession) {
    // Reset tracking and/or remove existing anchors if consistent tracking is required
    print("sessionInterruptionEnded")
  }
  
  override func becomeFirstResponder() -> Bool { return true }
  override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
    if motion == .motionShake {
      if !config.isBusy {
        if !config.initial.isInitialized || config.isPerforming {
          config.restart()
        }
      }
    }
  }
  
  override var childViewControllerForStatusBarStyle: UIViewController? { return config }
  override var prefersStatusBarHidden: Bool { return true }
  required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
