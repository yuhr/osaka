//
//  Extensions.swift
//  osaka
//
//  Created by yuhr on 2017/12/26.
//  Copyright © 2017 yuhr. All rights reserved.
//

import Foundation
import SceneKit
import UIKit
import ARKit

extension matrix_float4x4 {
  func offset() -> SCNVector3 {
    return SCNVector3(columns.3.x, columns.3.y, columns.3.z)
  }
  func scale() -> SCNVector3 {
    let sx = SCNVector3(columns.0.x, columns.0.y, columns.0.z)
    let sy = SCNVector3(columns.1.x, columns.1.y, columns.1.z)
    let sz = SCNVector3(columns.2.x, columns.2.y, columns.2.z)
    return SCNVector3(sx.length, sy.length, sz.length)
  }
}
infix operator ⋅
infix operator ⨯
infix operator ∡

extension SCNVector3{
  static func - (lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
    return SCNVector3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z)
  }
  
  static func + (lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
    return SCNVector3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z)
  }
  
  static func * (lhs: SCNVector3, rhs: Float) -> SCNVector3 {
    return SCNVector3(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs)
  }
  
  static func / (lhs: SCNVector3, rhs: Float) -> SCNVector3 {
    return SCNVector3(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs)
  }
  
  static func ⋅ (lhs: SCNVector3, rhs: SCNVector3) -> Float {
    return (lhs.x * rhs.x) + (lhs.y * rhs.y) + (lhs.z * rhs.z)
  }
  
  static func ⨯ (lhs: SCNVector3, rhs: SCNVector3) -> SCNVector3 {
    return SCNVector3(
      (lhs.y*rhs.z) - (lhs.z*rhs.y),
      (lhs.z*rhs.x) - (lhs.x*rhs.z),
      (lhs.x*rhs.y) - (lhs.y*rhs.x)
    )
  }
  
  static func ∡ (lhs: SCNVector3, rhs: SCNVector3) -> Float {
    return acos(lhs ⋅ rhs)
  }
  
  private var length²: Float {
    return (x*x) + (y*y) + (z*z)
  }
  
  var length: Float {
    return self.length².squareRoot()
  }
  
  var normalized: SCNVector3 {
    let length = self.length
    return SCNVector3(x: x/length, y: y/length, z: z/length)
  }
}

@discardableResult
func with<T>(_ obj: T, setup: (T) -> ()) -> T {
  setup(obj)
  return obj
}
precedencegroup PowerPrecedence {
  associativity: left
  higherThan: MultiplicationPrecedence
}
infix operator ** : PowerPrecedence
func ** (num: Double, power: Double) -> Double {
  return pow(num, power)
}
func ** (num: Int, power: Int) -> Int {
  return Int(pow(Double(num), Double(power)))
}
func ** (num: Float, power: Float) -> Float {
  return Float(pow(Double(num), Double(power)))
}

extension Float {
  func roundAtPlace(nth: Int) -> Float {
    let place = Float(10 ** nth)
    return roundf(self * place) / place
  }
}

extension UILabel {
  func setText(_ value: Any) {
    let text = String(describing: value)
    DispatchQueue.main.async {
      self.text = text
    }
  }
  func setColor(_ color: UIColor) {
    DispatchQueue.main.async {
      self.textColor = color
    }
  }
}



class PlaneNode: SCNNode {
  
  fileprivate override init() {
    super.init()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  var anchor: ARPlaneAnchor?
  var rot: SCNMatrix4 = SCNMatrix4MakeRotation(-.pi / 2, 1, 0, 0)
  
  init(anchor: ARPlaneAnchor) {
    super.init()
    
    self.anchor = anchor
    geometry = SCNPlane(width: CGFloat(anchor.extent.x), height: CGFloat(anchor.extent.z))
    let planeMaterial = SCNMaterial()
    planeMaterial.diffuse.contents = UIColor.white.withAlphaComponent(0.0)
    geometry?.materials = [planeMaterial]
    SCNVector3Make(anchor.center.x, 0, anchor.center.z)
    transform = rot
  }
  
  func update(anchor: ARPlaneAnchor) {
    self.anchor = anchor
    (geometry as! SCNPlane).width = CGFloat(anchor.extent.x)
    (geometry as! SCNPlane).height = CGFloat(anchor.extent.z)
    let tra = SCNMatrix4(anchor.transform)
    transform = SCNMatrix4Mult(rot, tra)
    //position = SCNVector3Make(anchor.center.x, 0, anchor.center.z)
  }
}

class DeviceNode: SCNNode {
  
  var available = false
  
  override init() {
    super.init()
    self.geometry = SCNSphere(radius: 0.01)
    self.name = "device"
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

class OriginNode: SCNNode {
  
  var available = false
  
  override init() {
    super.init()
    self.geometry = SCNSphere(radius: 0.01)
    self.name = "origin"
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

class Button: UIButton {
  
  private var _text: String = ""
  var text: String {
    get { return self._text }
    set(value) {
      self._text = value
      self.updateMasks(frame: self.frame)
    }
  }
  
  private var _color: UIColor = .white
  var color: UIColor {
    get { return self._color }
    set(value) {
      self._color = value
      self.updateMasks(frame: self.frame)
    }
  }
  
  override var frame: CGRect {
    get { return super.frame }
    set(frame) {
      super.frame = frame
      self.updateMasks(frame: frame)
    }
  }
  
  init() {
    super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    self.setup()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.setup()
  }
  
  private func updateMasks(frame: CGRect) {
    guard frame.size != CGSize.zero else { return }
    self.layer.borderColor = self.color.cgColor
    
    let bounds = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    
    UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0)
    let context = UIGraphicsGetCurrentContext()!
    context.scaleBy(x: 1, y: -1)
    context.translateBy(x: 0, y: -bounds.height)
    UIColor.black.setFill()
    UIBezierPath(rect: bounds).fill()
    
    // draw the text
    let attributes = [
      NSAttributedStringKey.font: UIFont.systemFont(ofSize: UIFont.buttonFontSize-2),
      NSAttributedStringKey.foregroundColor: UIColor.white
    ]
    let size = self.text.size(withAttributes: attributes)
    let point = CGPoint(x: (bounds.width - size.width) / 2.0, y: (bounds.height - size.height) / 2.0)
    self.text.draw(at: point, withAttributes: attributes)
    
    // create image mask
    let cgimage = UIGraphicsGetImageFromCurrentImageContext()!.cgImage!
    let mask = CGImage(maskWidth: cgimage.width, height: cgimage.height, bitsPerComponent: cgimage.bitsPerComponent, bitsPerPixel: cgimage.bitsPerPixel, bytesPerRow: cgimage.bytesPerRow, provider: cgimage.dataProvider!, decode: nil, shouldInterpolate: false)!
    UIGraphicsEndImageContext()
    
    // Set highlighted background image
    UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
    UIGraphicsGetCurrentContext()!.clip(to: bounds, mask: mask)
    self.color.setFill()
    UIBezierPath(rect: bounds).fill()
    self.setBackgroundImage(UIGraphicsGetImageFromCurrentImageContext()!, for: .highlighted)
    UIGraphicsEndImageContext()
    
    // Set normal background image
    UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
    let attributes2 = [
      NSAttributedStringKey.font: UIFont.systemFont(ofSize: UIFont.buttonFontSize-2),
      NSAttributedStringKey.foregroundColor: self.color
      ] as [NSAttributedStringKey : Any]
    self.text.draw(at: point, withAttributes: attributes2)
    self.setBackgroundImage(UIGraphicsGetImageFromCurrentImageContext()!, for: .normal)
    UIGraphicsEndImageContext()
  }
  
  func setup() {
    self.layer.borderWidth = 1
    self.layer.cornerRadius = 4
    self.layer.masksToBounds = true
    self.clipsToBounds = true
    self.setTitleColor(.clear, for: .normal)
    self.setTitleColor(.clear, for: .highlighted)
    self.updateMasks(frame: self.frame)
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

class ProgressView: UIProgressView {
  override func layoutSubviews() {
    super.layoutSubviews()
    if self.progressViewStyle == .default {
      subviews.forEach { subview in
        subview.layer.masksToBounds = true
        subview.layer.cornerRadius = self.frame.height / 2.0
      }
    }
  }
}
