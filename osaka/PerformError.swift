//
//  PerformError.swift
//  osaka
//
//  Created by yuhr on 2018/01/11.
//  Copyright © 2018 yuhr. All rights reserved.
//

import Foundation
import UIKit
//import AVFoundation

class PerformError: UIViewController {
  
  let labelMessage = UILabel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .orange
    
    self.view.addSubview(with(self.labelMessage) { label in
      label.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.width/2)
      label.center = self.view.center
      label.textColor = .white
      let text = "エラー状態です！\n受付にお戻しください。\n\nSomething went wrong!\nBring this device back\nto the front desk."
      label.numberOfLines = 0
      let style = NSMutableParagraphStyle()
      style.alignment = .center
      style.minimumLineHeight = 25.0
      style.maximumLineHeight = 25.0
      let attributedText = NSMutableAttributedString(string: text)
      attributedText.addAttribute(.paragraphStyle, value: style, range: NSMakeRange(0, attributedText.length))
      attributedText.addAttribute(.font, value: UIFont.systemFont(ofSize: 22), range: NSMakeRange(0, attributedText.length))
      label.attributedText = attributedText
      label.adjustsFontSizeToFitWidth = true
    })
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
  }
}
