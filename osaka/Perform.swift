//
//  Perform.swift
//  osaka
//
//  Created by yuhr on 2018/01/08.
//  Copyright © 2018 yuhr. All rights reserved.
//

import Foundation
import UIKit
import Interpolate

class Perform: UIPageViewController {
  
  private let normal = PerformNormal()
  private let error = PerformError()
  
  init() {
    super.init(transitionStyle: .scroll, navigationOrientation: .horizontal)
    self.modalPresentationStyle = .overCurrentContext
    self.modalPresentationCapturesStatusBarAppearance = true
    self.modalTransitionStyle = .crossDissolve
    self.setViewControllers([self.normal], direction: .reverse, animated: false)
  }
  
  var isWarning: Bool {
    get { return self.normal.isWarning }
    set(value) { self.normal.isWarning = value }
  }
  
  private var _isErrorOccurred: Bool = false
  var isErrorOccured: Bool {
    get { return self._isErrorOccurred }
    set(value) {
      if value != self._isErrorOccurred {
        self._isErrorOccurred = value
        print("isErrorOccurred = \(value)")
        if value {
          ar.stop()
          config.status = .error
          Audio.isPlayable = false
          DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            print("hello")
            self.setViewControllers([self.error], direction: .forward, animated: true) }
        } else {
          DispatchQueue.main.async { self.setViewControllers([self.normal], direction: .reverse, animated: false) }
        }
      }
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    config.setViewControllers([config.debug], direction: .forward, animated: false)
  }
  
  override var prefersStatusBarHidden: Bool { return true }
  required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

