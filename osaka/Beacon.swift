//
//  Beacon.swift
//  osaka
//
//  Created by yuhr on 2017/12/26.
//  Copyright © 2017 yuhr. All rights reserved.
//

import Foundation
import CoreBluetooth

class Beacon: ExpressibleByStringLiteral {
  
  var name: String = ""
  var rssi: Int = 0
  var txPower: Int = 0
  var namespaceID: String = ""
  var instanceID: String = ""
  
  init (_ name: String) { self.name = name }
  convenience required init(stringLiteral name: String) { self.init(name) }
  
  static var list: [String: Beacon] = [
    "4131366a3144": "Sikb", "6e467a636756": "M8mz", "744b7a594f6b": "7bJN",
    "6c3338586870": "N1lx", "4d4e726b684d": "0fQe", "596736773253": "754d",
    "326967774849": "5rca", "594b6c32776e": "UP63", "32743151576a": "yiUj",
    "38595736776e": "KtyS", "573468524447": "XG6s", "643534683336": "KeSS",
    "315841726136": "sBIh", "305a6243465a": "HDKN", "793843326746": "elSe",
    "4a6d65324346": "yycz", "443134506d48": "MS5M", "3055326b4e5a": "9bom",
    "4374304c5848": "38Zp", "66645a747a6b": "sFOB", "4c6f49565277": "hG2s",
    "757257476169": "Ex8v", "34335a6d3579": "kmrT", "4c7858716a4e": "mfLa",
    "7670546c6230": "fPPH", "327a5956486f": "d7gr", "413358533151": "2IRI",
    "674b56494a49": "4x4s", "47443662644d": "I3Cu", "4a7565446e57": "ldJg",
    "503876374d70": "Bqxg", "785036327946": "DNte", "6b716b6a5252": "E8KF",
    "6a7767355a62": "oM3h", "4950724d6c45": "2n4H", "534377614d4b": "dwq9",
    "31685a313941": "kIZe", "544e59506466": "XrnK", "335670717733": "eOoF",
    "3548496c6844": "3ggH", "467567706961": "kxpC", "777a337a4331": "O8ky",
    "795662695972": "N8xk", "517939436c6a": "DUX8", "4e3234377a6d": "vN2r",
    "527658344363": "D6cf", "365034643052": "SeWO", "4752315a7856": "kYTX",
    "444677384f70": "ee1p", "506f4d495a65": "cFcl", "61714b4c5242": "wH3G",
    "39796168344b": "QlZV", "594275665265": "fCOa", "324f4d57776d": "3qOA",
    "7879574b6273": "F8MT", "386f6b63716f": "21Qq", "6a3544707763": "2xYO",
    "4f4773383955": "sv8b", "4e584d6a6461": "1SfQ", "533174335a4a": "qI3e",
    "386c4d47756b": "Rjk5", "627076785835": "mgL7", "687952596237": "2A3z",
    "394d797a3938": "IN2Q", "734942494e4b": "zAuk", "4442556e6c6f": "2AOd",
    "6e3957463234": "7vio", "486d4c677238": "JmQp", "575551744457": "N2qs",
    "6b61636f4b52": "khnM", "777130626a51": "tFDk", "644c6e384459": "3H3C",
    "456131576236": "M0Jy", "735758623836": "z1YX", "67774738636d": "wDnU", // behind the dock
    // for test (battery beacons)
    "486743515474": "1V8G", "6f4c566c6a76": "3QhC", "69767a395a49": "4NUE",
    "636664383777": "L7ii", "4d4433563074": "NYG2", "6b6c44445879": "NrNZ"
  ]
  static var listDebug: [String: Beacon] = [
    // for test (battery beacons)
    "486743515474": Beacon.list["486743515474"]!, "6f4c566c6a76": Beacon.list["6f4c566c6a76"]!, "69767a395a49": Beacon.list["69767a395a49"]!,
    "636664383777": Beacon.list["636664383777"]!, "4d4433563074": Beacon.list["4d4433563074"]!, "6b6c44445879": Beacon.list["6b6c44445879"]!
  ]
  
  private var _accuracy: Double = 0.0
  var accuracy: Double {
    get { return self._accuracy }
  }
  var historyDistance: [Double] = []
  private var _distance: Double = 0.0
  var distance: Double {
    get { return self._distance }
    set(value) {
      self._distance = value
      self.historyDistance.append(value)
      if 10 < self.historyDistance.count {
        self.historyDistance.removeFirst()
      }
      
      self._distanceAverage = self.historyDistance.reduce(0.0) { $0 + $1 } / Double(self.historyDistance.count)
      
      self._accuracy = sqrt(self.historyDistance.map { distance in
        return (value-self.distanceAverage) ** 2
      }.reduce(0.0) { $0 + $1 } / Double(self.historyDistance.count))
    }
  }
  private var _distanceAverage: Double = 0.0
  var distanceAverage: Double {
    get { return self._distanceAverage }
  }
  
  func weight(in inCluster: BeaconCluster) -> Double {
    return 1.0 / (1.0 + abs(self.distance - inCluster.distanceAverage))
  }
}

class BeaconCluster {
  
  var beacons: [String: Beacon] = [:]
  
  var historyDistance: [Double] = []
  private var _distance: Double = 0.0
  var distance: Double {
    get { return self._distance }
    set(value) {
      self._distance = value
      self.historyDistance.append(value)
      let numDistances = 10*self.beacons.count
      if numDistances < self.historyDistance.count {
        self.historyDistance.removeFirst(self.historyDistance.count - numDistances)
      }
      
      self._distanceAverage = self.historyDistance.reduce(0.0) { $0 + $1 } / Double(self.historyDistance.count)
    }
  }
  private var _distanceAverage: Double = 0.0
  var distanceAverage: Double {
    get { return self._distanceAverage }
  }
  
  init() {}
  
  func updateDistance() {
    self.distance = self.beacons.reduce(0.0) { $0 + $1.value.distance * $1.value.weight(in: self) }
      / self.beacons.reduce(0.0) { $0 + $1.value.weight(in: self) }
  }
}

class BeaconManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
  
  static let shared = BeaconManager()
  private let uuid = CBUUID(string: "FEAA")
  
  private var central: CBCentralManager!
  private var timer: Timer!
  
  var callback: ((Beacon) -> ())? = nil
  
  private override init() {
    super.init()
    self.central = CBCentralManager()
    self.central.delegate = self
  }
  
  var scanInterval: TimeInterval = 0.1
  private var _isScanning: Bool = false
  private var _reservedStartScanning: Bool = false
  var isScanning: Bool {
    get { return self._isScanning }
    set(value) {
      guard self.isReady else {
        self._reservedStartScanning = true
        return
      }
      if value != self._isScanning {
        self._isScanning = value
        if !value {
          if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
          }
        } else if value {
          self._reservedStartScanning = false
          if self.timer == nil {
            self.timer = Timer.scheduledTimer(withTimeInterval: self.scanInterval, repeats: true) { _ in
              self.central.scanForPeripherals(withServices: [self.uuid])
            }
          }
        }
      }
    }
  }
  
  var isReady: Bool = false
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    if self.central.state == .poweredOn {
      self.isReady = true
      if self._reservedStartScanning { self.isScanning = true }
    }
  }
  
  func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
    guard let data = advertisementData[CBAdvertisementDataServiceDataKey] as? [CBUUID : NSData] else { return }
    guard let frame = data[self.uuid] else { return }
    guard frame.length == 18 else { return }
    var bytes = [UInt8](repeating: 0, count: frame.length)
    frame.getBytes(&bytes, length: frame.length)
    switch bytes[0] {
    case 0x00:
      let rssi = RSSI.intValue
      let txPower = Int(Int8(bitPattern: bytes[1]))
      let namespaceID: String = bytes[2..<12].reduce("") { return $0 + String(format: "%02x", $1) }
      let instanceID: String = bytes[12..<18].reduce("") { return $0 + String(format: "%02x", $1) }
      var beacon: Beacon
      if let known = Beacon.list[instanceID] {
        beacon = known
        Beacon.list[instanceID] = beacon
      } else {
        beacon = "unknown"
        Beacon.list[instanceID] = beacon
      }
      beacon.rssi = rssi
      beacon.txPower = txPower
      beacon.instanceID = instanceID
      beacon.namespaceID = namespaceID
      beacon.distance = 10.0 ** (Double(txPower - rssi) / (10.0 * 2.0)) / 100.0
      if let callback = self.callback {
        callback(beacon)
      }
    default: break
    }
  }
}
