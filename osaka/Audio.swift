//
//  Audio.swift
//  osaka
//
//  Created by yuhr on 2017/12/26.
//  Copyright © 2017 yuhr. All rights reserved.
//

import Foundation
import AVFoundation
import Interpolate
import UIKit
import SceneKit
import Regex

class AudioTrack: NSObject, AVAudioPlayerDelegate, ExpressibleByStringLiteral {
  
  let name: String
  var player: AVAudioPlayer!
  var fade: Interpolate!
  var audio: Audio!
  
  init(_ path: String) {
    let url = URL(fileURLWithPath: path, isDirectory: false)
    let ext = url.pathExtension
    self.name = url.lastPathComponent
    let name = url.deletingPathExtension().relativeString.removingPercentEncoding!
    let audioPath = Bundle.main.path(forResource: name, ofType: ext)!
    let audioUrl = URL(fileURLWithPath: audioPath)
    super.init()
    do {
      player = try AVAudioPlayer(contentsOf: audioUrl)
    } catch let error {
      print("audio file error: \(error.localizedDescription)")
      player = nil
      return
    }
    player.delegate = self
    player.numberOfLoops = -1 // infinity loop
    player.volume = 0.0
    player.prepareToPlay()
    player.currentTime = 0.0
    fade = Interpolate(from: Double(0.0), to: Double(1.0), apply: { (volume: Double) in
      self.audio.volume = Float(volume) * Audio.masterVolume
    })
    print("audio file ok: \(path)")
  }
  convenience required init(stringLiteral path: String) { self.init(path) }
}

class Audio {
  
  var track: AudioTrack
  var cell: AudioTableCell!
  let node: SCNNode
  let index: Int
  private var factor: Float = 0.0
  static var isAnalog = false
  static var isParabola = true
  static var isTriggered = false
  static var masterVolume: Float = 0.0
  
  static func format(time: TimeInterval) -> String {
    let floored: Int = Int(floor(time))
    //let h: Int = floored / 3600
    let m: Int = floored % 3600 / 60
    let s: Int = floored % 3600 % 60
    //return String(format: "%d:%02d:%02d", h, m, s)
    return String(format: "%d:%02d", m, s)
  }
  
  private var lastVolumes: [Float] = []
  var volume: Float {
    get { return (Audio.masterVolume != 0.0 ?  self.track.player.volume / Audio.masterVolume : 0.0) }
    set(value) {
      self.lastVolumes.append(volume)
      if 10 < self.lastVolumes.count {
        self.lastVolumes.removeFirst()
      }
      self.track.player.volume = value * Audio.masterVolume
      if volume == 0.0 {
        if self.track.player.isPlaying {
          self.track.player.pause()
          DispatchQueue.main.async {
            if self.cell != nil {
              if let meter = self.cell.meters["Playback"] {
                DispatchQueue.main.async { meter.progressTintColor = .white }
              }
            }
          }
        }
      } else {
        if !self.track.player.isPlaying {
          self.track.player.play()
          DispatchQueue.main.async {
            if self.cell != nil {
              if let meter = self.cell.meters["Playback"] {
                DispatchQueue.main.async { meter.progressTintColor = .green }
              }
            }
          }
        }
      }
      guard self.cell != nil else { return }
      if let meter = self.cell.meters["Volume"] {
        DispatchQueue.main.async { meter.setProgress(value, animated: false) }
      }
      if let label = self.cell.labels["Volume"] {
        label.setText(String(format: "%0.1f %%", value.roundAtPlace(nth: 3) * 100.0))
      }
    }
  }
  
  static var fadingDuration: Float = 2.0
  static let fadingDurationMax: Float = 2.0
  var isPlaying: Bool = false {
    willSet(value) {
      if value != self.isPlaying {
        config.impact()
        if let cell = self.cell {
          cell.labelTrackName.setColor(value ? .green : .white)
        }
        self.track.fade.stopAnimation()
        self.track.fade.progress = CGFloat(self.volume)
        self.track.fade.animate(CGFloat(value ? 1.0 : 0.0), duration: CGFloat(Audio.fadingDuration))
      }
      guard self.cell != nil else { return }
      let curr = self.track.player.currentTime
      if let meter = self.cell.meters["Playback"] {
        DispatchQueue.main.async { meter.progress = Float(curr / self.track.player.duration) }
      }
      if let label = self.cell.labels["Playback"] {
        label.setText(Audio.format(time: curr))
      }
    }
  }
  
  var proxZ: Float {
    get { return Float(cell.labels["ProxZ"]!.text!)! }
    set(value) {
      self.factor = (Audio.lengthAreaHalf - min(max(0.0, value), Audio.lengthAreaHalf)) / Audio.lengthAreaHalf
      guard cell != nil, let proxZ = cell.labels["ProxZ"] else { return }
      proxZ.setText(String(format: "%0.3f m", max(0.0, value-self.lengthArea/2.0).roundAtPlace(nth: 3)))
      proxZ.setColor(value < self.lengthArea/2.0 ? .green : .white)
    }
  }
  
  static let lengthCorridorActual: Float = 35.86
  static let ratioShorten: Float = 0.02
  static let lengthFactor: Float = 1.0 - Audio.ratioShorten
  static let lengthCorridorEstimated: Float = Audio.lengthCorridorActual * Audio.lengthFactor
  static let numAreas: Int = Audio.fileNames.filter { return $0 =~ "^\\d+.+\\.(caf|wav)$" }.count
  static let ratioOverlapped: Float = 0.25
  static let ratioNotOverlapped: Float = 1.0 - (Audio.ratioOverlapped * 2.0)
  static let ratioStride: Float = Audio.ratioNotOverlapped + Audio.ratioOverlapped
  static let ratioCorridor: Float = Audio.ratioStride * Float(Audio.numAreas) - Audio.ratioOverlapped
  static let lengthArea: Float = Audio.lengthCorridorEstimated / Audio.ratioCorridor // 2.561429
  static let lengthAreaHalf: Float = Audio.lengthArea / 2.0
  static let lengthStride: Float = Audio.lengthArea * Audio.ratioStride
  static let lengthOverlapped: Float = Audio.lengthArea * Audio.ratioOverlapped
  static let lengthNotOverlapped: Float = Audio.lengthArea * Audio.ratioNotOverlapped
  static let lengthEntireArea = Audio.lengthCorridorEstimated + Audio.lengthOverlapped * 2.0
  static let lengthAreaPositionOffset: Float = 2.5 - Audio.lengthOverlapped - Audio.lengthAreaHalf
  var lengthAreaPositionZ: Float {
    switch self.index {
    case 0:
      return Audio.lengthAreaPositionOffset + Audio.lengthAreaHalf + Float(self.index) * Audio.lengthStride - 0.75/2.0
    case Audio.numAreas-1:
      return Audio.lengthAreaPositionOffset + Audio.lengthAreaHalf + Float(self.index) * Audio.lengthStride + 100.0/2.0
    default:
      return Audio.lengthAreaPositionOffset + Audio.lengthAreaHalf + Float(self.index) * Audio.lengthStride
    }
  }
  var lengthArea: Float {
    switch self.index {
    case 0:
      return Audio.lengthArea + 0.75
    case Audio.numAreas-1:
      return Audio.lengthArea + 100.0
    default:
      return Audio.lengthArea
    }
  }
  
  func rearrange(at: SCNVector3, rotationY: Float) {
    var transform = SCNMatrix4MakeTranslation(0.0, 0.0, -self.lengthAreaPositionZ)
    transform = SCNMatrix4Rotate(transform, rotationY, 0.0, 1.0, 0.0)
    transform = SCNMatrix4Translate(transform, at.x, at.y, at.z)
    self.node.transform = transform
  }
  
  init(index: Int, name: String, track: AudioTrack) {
    self.index = index
    let box = SCNBox(width: 3.0, height: 3.0, length: CGFloat(Audio.lengthArea), chamferRadius: 0.0)
    self.node = SCNNode(geometry: box)
    let material = SCNMaterial()
    material.diffuse.contents = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)
    self.track = track
    self.node.geometry?.materials = [material]
    self.node.name = name
    self.rearrange(at: SCNVector3Zero, rotationY: 0.0)
    box.length = CGFloat(self.lengthArea)
    self.track.audio = self
  }
  
  static let fileNames: [String] = {
    do {
      return try FileManager.default.contentsOfDirectory(atPath: Bundle.main.bundlePath + "/audio/")
        .filter { return $0 =~ "^\\d+.+\\.(caf|wav)$" }
        .sorted {
          let a = Int("^(\\d+).+$".r!.findFirst(in: $0)!.group(at: 1)!)!
          let b = Int("^(\\d+).+$".r!.findFirst(in: $1)!.group(at: 1)!)!
          return a < b
        }
    } catch { return [] }
  }()
  static let list: [Audio] = Audio.fileNames.map { fileName in
    let path = "audio/\(fileName)"
    let index = Int("^(\\d+).+$".r!.findFirst(in: fileName)!.group(at: 1)!)! - 1
    return Audio(
      index: index,
      name: path,
      track: AudioTrack(path))
  }
  
  func updateInfo() {
    if let cell = self.cell {
      if let label = cell.labels["Playback"] {
        label.setText(Audio.format(time: self.track.player.currentTime))
      }
      if let label = cell.labels["Duration"] {
        label.setText(Audio.format(time: self.track.player.duration))
      }
      if let label = cell.labels["Samples"] {
        label.setText(String(format: "%d kHz", Int(self.track.player.format.streamDescription.pointee.mSampleRate / 1000.0)))
      }
      if let label = cell.labels["Bits"] {
        label.setText(String(format: "%d bpc", Int(self.track.player.format.streamDescription.pointee.mBitsPerChannel)))
      }
      cell.labelTrackName.setText(self.track.name)
    }
  }
  
  static var isInterrupted = false
  static private var fadePlayable = Interpolate(from: Double(0.0), to: Double(1.0), apply: { (volume: Double) in
    Audio.masterVolume = Float(volume)
  })
  static private var _isPlayable: Bool = false
  static var isPlayable: Bool {
    get {
      return self._isPlayable
    }
    set(value) {
      if value && !self._isPlayable {
        self._isPlayable = true
        self.isInterrupted = false
        for audio in Audio.list {
          audio.volume = 0.0
          audio.track.player.currentTime = 0.0
        }
        Audio.fadePlayable.stopAnimation()
        Audio.fadePlayable.progress = 0.0
        Audio.fadePlayable.animate(duration: 0.5)
      } else if !value && self._isPlayable {
        self._isPlayable = false
        self.isInterrupted = true
        Audio.fadePlayable.stopAnimation()
        Audio.fadePlayable.progress = 1.0
        Audio.fadePlayable.animate(0.0, duration: 0.5, completion: {
          for audio in Audio.list {
            audio.volume = 0.0
            audio.track.player.currentTime = 0.0
          }
        })
      }
    }
  }
  
  static func rearrange(at: SCNVector3, rotationY: Float) {
    for audio in Audio.list {
      audio.rearrange(at: at, rotationY: rotationY)
    }
  }
  
  static func deviceMoving(at: SCNVector3) {
    for audio in Audio.list {
      let d = audio.node.position - at
      let dHorizontal = SCNVector3(d.x, 0.0, d.z)
      let distZ = abs(ar.zDirection ⋅ dHorizontal)
      audio.proxZ = distZ
      if self.isInterrupted { continue }
      audio.isPlaying = distZ <= audio.lengthArea/2.0
    }
  }
}
