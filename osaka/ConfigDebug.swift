//
//  ConfigDebug.swift
//  osaka
//
//  Created by yuhr on 2017/12/26.
//  Copyright © 2017 yuhr. All rights reserved.
//

import Foundation
import UIKit
import SceneKit
import ARKit
import MediaPlayer
import ActionClosurable
import Mute

class ConfigDebug: UIViewController, ARSCNViewDelegate, UITableViewDelegate, UITableViewDataSource {
  
  var viewTable = UITableView()
  var labels: [String: UILabel] = [
    "Device": UILabel(),
    "NumFtPts": UILabel(),
    "FromOrig": UILabel(),
    "FromPrev": UILabel(),
    "Status": UILabel(),
    "Shorter": UILabel(),
    "Fading": UILabel(),
    "Lightness": UILabel()
  ]
  var cells: [AudioTableCell] = []
  
  let viewLabels = UIView()
  
  let feedbackSelection = UISelectionFeedbackGenerator()
  let feedbackImpact = UIImpactFeedbackGenerator(style: .heavy)
  
  var height: Int { return 40 + 25 * self.numLabelsLeft }
  var numLabelsLeft: Int { return self.labels.count / 2 + self.labels.count % 2 }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Setup UITableView
    with(self.viewTable) { table in
      table.register(AudioTableCell.self, forCellReuseIdentifier: "audio")
      table.frame = CGRect(x: 0.0, y: 0.0, width: Double(self.view.frame.width), height: Double(self.view.frame.height))
      table.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
      table.separatorColor = UIColor(white: 1.0, alpha: 0.25)
      table.allowsSelection = false
      table.separatorInset = .zero
      table.delaysContentTouches = false
      table.delegate = self
      table.dataSource = self
    }
    
    // Separator between the header and the first cell
    let header = UIView(frame: CGRect(x: 0.0, y: 0.0, width: Double(self.view.frame.width), height: Double(self.height)+0.5))
    header.addSubview(with(UIView()) { border in
      border.frame = CGRect(x: 0.0, y: Double(height), width: Double(self.view.frame.width), height: 0.5)
      border.backgroundColor = self.viewTable.separatorColor
    })
    
    let fontStatus = UIFont.monospacedDigitSystemFont(ofSize: 14, weight: .regular)
    
    header.addSubview(with(self.viewLabels) { view in
      view.frame = CGRect(x: 25.0, y: 20.0, width: Double(header.frame.width) - 25.0 * 2.0, height: 25.0 * Double(self.numLabelsLeft))
      let widthCaption = 70.0
      let widthValue = 65.0
      let widthEachItem = widthCaption + widthValue
      let offsetEachItem = (Double(view.frame.width)/2.0-Double(widthEachItem))/2.0
      view.addSubview(with(UIView()) { line in
        let length: Double = Double(view.frame.height) - 10.0
        line.frame = CGRect(x: Double(view.frame.width)/2.0, y: 5.0, width: 0.5, height: length)
        line.backgroundColor = UIColor(white: 1.0, alpha: 0.25)
      })
      for (offset: index, element: (key: name, value: value)) in labels.enumerated() {
        let isLeftRow = index < self.numLabelsLeft
        let xCaption = (isLeftRow ? offsetEachItem : Double(view.frame.width)/2.0+offsetEachItem)
        let xValue = xCaption + widthCaption
        let y = 25.0 * (Double(index) - (isLeftRow ? 0.0 : Double(self.numLabelsLeft)))
        
        view.addSubview(with(UILabel()) { label in
          label.frame = CGRect(x: xCaption, y: y, width: widthCaption, height: 25.0)
          label.text = "\(name):"
          label.font = fontStatus
          label.textAlignment = .right
          label.textColor = .white
        })
        view.addSubview(with(value) { label in
          label.frame = CGRect(x: xValue, y: y, width: widthValue, height: 25.0)
          label.text = "N/A"
          label.font = fontStatus
          label.textAlignment = .right
          label.textColor = .white
        })
      }
    })
    
    viewTable.tableHeaderView = header
    self.view.addSubview(viewTable)
    
    self.labels["Shorter"]!.setText(String(format: "%0.1f %%", Audio.ratioShorten * 100.0))
    self.labels["Fading"]!.setText(String(format: "%0.3f s", Audio.fadingDuration))
    self.labels["Device"]!.setText(UIDevice.current.name)
    
    for _ in Audio.list {
      cells.append(AudioTableCell(style: UITableViewCellStyle.default, reuseIdentifier: "audio"))
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.isUserInteractionEnabled = true
    
    BeaconManager.shared.isScanning = false
    BeaconManager.shared.callback = nil
    config.beaconCluster.beacons.removeAll()
    config.beaconCluster.historyDistance.removeAll()
    config.beaconTimeFirstNear = nil
    config.beaconTimeFirstFar = nil
    config.status = .normal
    
    BeaconManager.shared.callback = { beacon in
      guard ((Beacon.listDebug as NSDictionary).allKeys(for: beacon) as! [String]).count != 0 else { return }
      if beacon.distanceAverage < config.beaconThresholdDistanceFarDebug &&
        0.00001 < beacon.distanceAverage {
        config.beaconCluster.beacons[beacon.instanceID] = beacon
        config.beaconCluster.updateDistance()
      } else {
        let keys = ((config.beaconCluster.beacons as NSDictionary).allKeys(for: beacon) as! [String])
        if 0 < keys.count {
          config.beaconCluster.beacons.removeValue(forKey: keys[0])
        }
        let numDistances = 10*config.beaconCluster.beacons.count
        if numDistances < config.beaconCluster.historyDistance.count {
          config.beaconCluster.historyDistance.removeFirst(config.beaconCluster.historyDistance.count - numDistances)
        }
        if numDistances == 0 {
          config.beaconTimeFirstNear = nil
          config.isPerforming = config.initial.isInitialized
        }
      }
      if config.beaconCluster.distanceAverage < config.beaconThresholdDistanceNearDebug {
        config.beaconTimeFirstFar = nil
        if config.beaconTimeFirstNear == nil {
          config.beaconTimeFirstNear = Date()
        } else if config.beaconThresholdDurationDebug < abs(config.beaconTimeFirstNear!.timeIntervalSinceNow) {
          config.isPerforming = false
        }
      } else {
        config.beaconTimeFirstNear = nil
        if config.beaconTimeFirstFar == nil {
          config.beaconTimeFirstFar = Date()
        } else if config.beaconThresholdDurationDebug < abs(config.beaconTimeFirstFar!.timeIntervalSinceNow) {
          config.isPerforming = config.initial.isInitialized
        }
      }
    }
    BeaconManager.shared.isScanning = true
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    config.beaconCluster.beacons.removeAll()
    config.beaconCluster.historyDistance.removeAll()
    config.beaconTimeFirstNear = nil
    config.beaconTimeFirstFar = nil
  }
  
  func tableView(_ tableView: UITableView, numberOfSection section: Int) -> Int { return 1 }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return Audio.list.count }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let i: Int = indexPath.row
    let cell = cells[i]
    cell.labelIndex.text = String(i+1)
    let audio = Audio.list[i]
    audio.cell = cell
    audio.updateInfo()
    if let label = audio.cell.labels["Volume"] {
      label.setText(String(format: "%0.1f %%", 0.0 * 100.0))
    }
    if let label = audio.cell.labels["OffsetZ"] {
      label.setText(String(format: "%0.3f m", audio.lengthAreaPositionZ - audio.lengthArea/2.0))
    }
    audio.cell.labelTrackName.setText(audio.track.name)
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let cell = cells[indexPath.row]
    return CGFloat(cell.height)
  }
}

class AudioTableCell: UITableViewCell {
  
  let labelIndex = UILabel()
  let labelTrackName = UILabel()
  let labels: [String: UILabel] = [
    "ProxZ": UILabel(),
    "Volume": UILabel(),
    "Playback": UILabel(),
    "Duration": UILabel(),
    "OffsetZ": UILabel(),
    "Samples": UILabel(),
    "Bits": UILabel()
  ]
  let meters: [String: ProgressView] = [
    "Volume": ProgressView(),
    "Playback": ProgressView()
  ]
  let viewMeters = UIView()
  let viewLabels = UIView()
  
  var height: Int { return 40 + 25 * self.numLabelsLeft + 20 * self.meters.count + 20 + 25 }
  var numLabelsLeft: Int { return self.labels.count / 2 + self.labels.count % 2 }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    let fontIndex = UIFont.monospacedDigitSystemFont(ofSize: 32, weight: .ultraLight)
    let fontTitle = UIFont.monospacedDigitSystemFont(ofSize: 16, weight: .light)
    let fontStatus = UIFont.monospacedDigitSystemFont(ofSize: 14, weight: .regular)
    let fontMeter = UIFont.monospacedDigitSystemFont(ofSize: 11, weight: .regular)
    
    self.backgroundColor = .clear
    self.layoutMargins = .zero
    self.frame = CGRect(x: 0.0, y: 0.0, width: Double(config.view.frame.width), height: Double(self.height))
    
    self.contentView.addSubview(with(self.labelIndex) { label in
      label.frame = CGRect(x: 0.0, y: 19.0, width: 45.0, height: 40.0)
      label.text = "0"
      label.font = fontIndex
      label.textAlignment = .right
      label.textColor = .white
    })
    self.contentView.addSubview(with(UIView()) { line in
      let length: Int = self.height - 40 - 10
      line.frame = CGRect(x: 50.0, y: Double(self.height - length)/2.0, width: 0.5, height: Double(length))
      line.backgroundColor = UIColor(white: 1.0, alpha: 0.25)
    })
    self.contentView.addSubview(with(self.labelTrackName) { label in
      label.frame = CGRect(x: 70, y: 20, width: Int(self.frame.width)-70-20, height: 25)
      label.text = "N/A"
      label.font = fontTitle
      label.textAlignment = .right
      label.textColor = .white
    })
    
    self.contentView.addSubview(with(self.viewMeters) { view in
      view.frame = CGRect(x: 70.0, y: 55.0, width: Double(self.frame.width)-90.0, height: 20.0 * Double(self.meters.count))
      for (offset: index, element: (key: name, value: value)) in self.meters.enumerated() {
        let y = 20 * index
        
        view.addSubview(with(UILabel()) { label in
          label.frame = CGRect(x: 0, y: y, width: 60, height: 20)
          label.text = "\(name):"
          label.font = fontMeter
          label.textAlignment = .right
          label.textColor = .white
        })
        view.addSubview(with(value) { meter in
          meter.frame = CGRect(x: 70.0, y: Double(y)+10.0, width: Double(view.frame.width)-70.0, height: 20.0)
          meter.trackTintColor = UIColor(white: 1.0, alpha: 0.2)
          meter.progressTintColor = .green
          meter.progressViewStyle = .default // .bar for squared look
          meter.progress = 0.0
        })
      }
    })
    
    self.contentView.addSubview(with(self.viewLabels) { view in
      view.frame = CGRect(x: 55.0, y: Double(self.viewMeters.frame.height)+65.0, width: Double(config.view.frame.width)-55.0-20.0, height: 25.0 * Double(self.numLabelsLeft))
      let widthCaption = 70.0
      let widthValue = 65.0
      let widthEachItem = widthCaption + widthValue
      let offsetEachItem = (Double(view.frame.width)/2.0-Double(widthEachItem))/2.0
      view.addSubview(with(UIView()) { line in
        let length: Double = Double(view.frame.height) - 10.0
        line.frame = CGRect(x: Double(view.frame.width)/2.0, y: 5.0, width: 0.5, height: length)
        line.backgroundColor = UIColor(white: 1.0, alpha: 0.25)
      })
      for (offset: index, element: (key: name, value: value)) in labels.enumerated() {
        let isLeftRow = index < self.numLabelsLeft
        let xCaption = (isLeftRow ? offsetEachItem : Double(view.frame.width)/2.0+offsetEachItem)
        let xValue = xCaption + widthCaption
        let y = 25.0 * (Double(index) - (isLeftRow ? 0.0 : Double(self.numLabelsLeft)))
        
        view.addSubview(with(UILabel()) { label in
          label.frame = CGRect(x: xCaption, y: y, width: widthCaption, height: 25.0)
          label.text = "\(name):"
          label.font = fontStatus
          label.textAlignment = .right
          label.textColor = .white
        })
        view.addSubview(with(value) { label in
          label.frame = CGRect(x: xValue, y: y, width: widthValue, height: 25.0)
          label.text = "N/A"
          label.font = fontStatus
          label.textAlignment = .right
          label.textColor = .white
        })
      }
    })
  }
  
  required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
}
