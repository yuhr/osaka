//
//  ConfigInit.swift
//  osaka
//
//  Created by yuhr on 2018/01/08.
//  Copyright © 2018 yuhr. All rights reserved.
//

import Foundation
import UIKit
import M13Checkbox
import Interpolate
import AVFoundation
import CoreMotion

class ConfigInit: UIViewController {
  
  private let checkbox = M13Checkbox()
  private let labelWaiting = UILabel()
  private let labelBattery = UILabel()
  private let color = UIColor(white: 0.0, alpha: 0.5)
  private let activity = UIActivityIndicatorView(activityIndicatorStyle: .gray)
  
  private var fadeWaiting: Interpolate!
  private var fadeBattery: Interpolate!
  private var fade: Interpolate!
  
  private var motion = CMMotionManager()
  
  var isInitializing = false {
    didSet(oldValue) {
      if oldValue != self.isInitializing {
        print("isInitializing = \(self.isInitializing)")
        if !self.isInitialized {
          if self.isInitializing {
            self.fadeWaiting.animate(0.0, duration: 0.3)
            self.fadeBattery.animate(0.0, duration: 0.3, completion: {
              self.activity.startAnimating()
              ar.start()
            })
          } else {
            self.isInitialized = false
            ar.stop()
            self.activity.stopAnimating()
            self.fadeWaiting.animate(1.0, duration: 0.3)
            self.fadeBattery.animate(1.0, duration: 0.3)
            self.checkbox.setCheckState(.unchecked, animated: true)
          }
        }
      }
    }
  }
  var isInitialized = false {
    didSet(oldValue) {
      if oldValue != self.isInitialized {
        print("isInitialized = \(self.isInitialized)")
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    
    self.view.addSubview(with(self.checkbox) { check in
      check.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width/2+60, height: self.view.frame.width/2+60)
      check.center = self.view.center
      check.stateChangeAnimation = .stroke
      check.animationDuration = 0.3
      check.secondaryTintColor = .clear
      check.isUserInteractionEnabled = false
    })
    
    self.view.addSubview(with(self.labelWaiting) { label in
      label.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.width/2)
      label.center = self.view.center
      label.textColor = color
      let text = "エラー状態です！\n受付にお戻しください。\n\nSomething went wrong!\nBring this device back\nto the front desk."
      label.numberOfLines = 0
      let style = NSMutableParagraphStyle()
      style.alignment = .center
      style.minimumLineHeight = 25.0
      style.maximumLineHeight = 25.0
      let attributedText = NSMutableAttributedString(string: text)
      attributedText.addAttribute(.paragraphStyle, value: style, range: NSMakeRange(0, attributedText.length))
      attributedText.addAttribute(.font, value: UIFont.systemFont(ofSize: 22), range: NSMakeRange(0, attributedText.length))
      label.attributedText = attributedText
    })
    
    self.view.addSubview(with(self.labelBattery) { label in
      label.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.width/2)
      label.text = "N/A %"
      label.textAlignment = .center
      label.center = CGPoint(x: self.view.center.x, y: self.view.frame.height - 40)
      label.textColor = color
      label.font = .systemFont(ofSize: 22)
    })
    
    self.view.addSubview(with(self.activity) { activity in
      activity.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
      activity.hidesWhenStopped = true
    })
    
    self.fadeWaiting = Interpolate(from: UIColor.clear, to: self.color, apply: { color in
      self.labelWaiting.textColor = color
    })
    self.fadeWaiting.progress = 1.0
    self.fadeBattery = Interpolate(from: UIColor.clear, to: self.color, apply: { color in
      self.labelBattery.textColor = color
    })
    self.fadeBattery.progress = 1.0
    
    self.fade = Interpolate(from: UIColor.white, to: .green, apply: { color in
      self.view.backgroundColor = color
    })
    UIDevice.current.isBatteryMonitoringEnabled = true
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIDeviceBatteryStateDidChange, object: nil, queue: nil) { _ in
      self.scanBatteryState()
    }
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIDeviceBatteryLevelDidChange, object: nil, queue: nil) { _ in
      self.scanBatteryLevel()
    }
    self.scanBatteryState()
    self.scanBatteryLevel()
  }
  
  private func scanBatteryState() {
    switch UIDevice.current.batteryState {
    case .unplugged:
      self.fade.stopAnimation()
      self.fade.animate(0.0, duration: 0.3)
    case .charging:
      self.fade.stopAnimation()
      self.fade.animate(1.0, duration: 0.3)
    default: break
    }
  }
  private func scanBatteryLevel() {
    self.labelBattery.setText(String(format: "%d %%", Int(UIDevice.current.batteryLevel*100.0)))
  }
  
  private var firstDateHorizontal: Date?
  private var firstDateStill: Date?
  private var isHorizontal = false {
    didSet(oldValue) {
      if oldValue != self.isHorizontal {
        //print("isHorizontal = \(self.isHorizontal)")
        self.firstDateHorizontal = self.isHorizontal ? Date() : nil
      }
    }
  }
  private var isStill = false {
    didSet(oldValue) {
      if oldValue != self.isStill {
        //print("isStill = \(self.isStill)")
        self.firstDateStill = self.isStill ? Date() : nil
      }
    }
  }
  private var _isSteady: Bool = false
  var isSteady: Bool {
    get { return self._isSteady }
    set(value) {
      if value != self._isSteady {
        self._isSteady = value
        //print("isSteady = \(value)")
      }
    }
  }
  var lastAttitude: CMAttitude!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.checkbox.setCheckState(.unchecked, animated: false)
    self.fadeWaiting.progress = 1.0
    self.fadeBattery.progress = 1.0
    self.scanBatteryState()
    
    BeaconManager.shared.isScanning = false
    BeaconManager.shared.callback = nil
    config.beaconCluster.beacons.removeAll()
    config.beaconCluster.historyDistance.removeAll()
    config.beaconTimeFirstNear = nil
    config.beaconTimeFirstFar = nil
    self.isInitialized = false
    
    motion.deviceMotionUpdateInterval = 0.1
    motion.startDeviceMotionUpdates(to: OperationQueue.current!) { motion, _ in
      if let att = motion?.attitude {
        self.isHorizontal = abs(att.roll) < 0.025 && 0.0 <= att.pitch
        let pitch = (att.pitch-(self.lastAttitude != nil ? self.lastAttitude.pitch : 0.0))
        let roll = (att.roll-(self.lastAttitude != nil ? self.lastAttitude.roll : 0.0))
        let yaw = (att.yaw-(self.lastAttitude != nil ? self.lastAttitude.yaw : 0.0))
        self.isStill = (pitch + roll + yaw) < 0.001
        if let durationHorizontal = self.firstDateHorizontal?.timeIntervalSinceNow,
           let durationStill = self.firstDateStill?.timeIntervalSinceNow {
          self.isSteady = 1.0 <= abs(durationHorizontal) && 1.0 <= abs(durationStill)
        } else {
          self.isSteady = false
        }
        self.lastAttitude = att
      }
    }
    
    BeaconManager.shared.callback = { beacon in
      if beacon.distanceAverage < config.beaconThresholdDistanceFarInit &&
        0.00001 < beacon.distanceAverage {
        config.beaconCluster.beacons[beacon.instanceID] = beacon
        config.beaconCluster.updateDistance()
      } else {
        let keys = ((config.beaconCluster.beacons as NSDictionary).allKeys(for: beacon) as! [String])
        if 0 < keys.count {
          config.beaconCluster.beacons.removeValue(forKey: keys[0])
        }
        let numDistances = 10*config.beaconCluster.beacons.count
        if numDistances < config.beaconCluster.historyDistance.count {
          config.beaconCluster.historyDistance.removeFirst(config.beaconCluster.historyDistance.count - numDistances)
        }
        if numDistances == 0 {
          config.beaconTimeFirstNear = nil
          if self.isInitialized {
            Audio.isPlayable = true
            config.isPerforming = true
            config.status = .normal
          } else {
            self.isInitializing = false
          }
        }
      }
      if config.beaconCluster.distanceAverage < config.beaconThresholdDistanceNearInit {
        config.beaconTimeFirstFar = nil
        if config.beaconTimeFirstNear == nil {
          config.beaconTimeFirstNear = Date()
        } else if config.beaconThresholdDurationInit < abs(config.beaconTimeFirstNear!.timeIntervalSinceNow) {
          self.isInitializing = self.isSteady
        }
      } else {
        config.beaconTimeFirstNear = nil
        if config.beaconTimeFirstFar == nil {
          config.beaconTimeFirstFar = Date()
        } else if config.beaconThresholdDurationInit < abs(config.beaconTimeFirstFar!.timeIntervalSinceNow) {
          if self.isInitialized {
            Audio.isPlayable = true
            config.isPerforming = true
            config.status = .normal
          } else {
            self.isInitializing = false
          }
        }
      }
    }
    BeaconManager.shared.isScanning = true
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    config.isPerforming = false
  }
  
  func check() {
    if self.isInitializing {
      self.isInitialized = true
      performance.isErrorOccured = false
      config.isInitial = false
      motion.stopDeviceMotionUpdates()
      self.activity.stopAnimating()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        self.checkbox.setCheckState(.checked, animated: true)
      }
    }
  }
}
